const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Product = require('../models/Product');



//s42 Create User
module.exports.registerUser = async (reqBody) => {
    let checkEmailExists = await User.find({
        email: reqBody.email
    })
    if (checkEmailExists.length){
        return  "Email is Already Taken"
    } else{
    let newUser = new User({
         email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })
    return await newUser.save().then((user, error) =>{
        if(error){
            return false
        } else {
            return "User Has been Registered Successfully"
        }
    })
}
}

//s42 loginUser
module.exports.loginRegUser = (reqBody) => {
    return User.findOne({
        email: reqBody.email
    })
    .then(result => {
        if (result == null){
            return false
        } else{
            //compareSync(<data to compare>, <encrypted password>)
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
            if(isPasswordCorrect){
                return {access: auth.createAccessToken(result)}
            } else{
                return false
            }
        }
    })
};


////s45 Retrieve User Details
module.exports.getUserDetails = (userData) => {
    return User.findById(userData.id)
    .then(result => {
        console.log(userData)
        if(result == null) {
          return false
        }else {
        console.log(result)
        result.password="******";
        return result
        }
    })
}; 

//Stretch Goal: Set user to Admin 



module.exports.updateProduct = async (reqParams, reqBody, userData) =>{
   
    if(userData.isAdmin === true) {
        let updatedProduct = {
            name: reqBody.name,
            description: reqBody.description,
            category: reqBody.category,
            price: reqBody.price

        }
        //Syntax: findByIdAndUpdate(document ID, updatesToBeApplied)
        return await Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
        .then((updatedProduct, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })
    } else {
       return await "You are not an Admin"
    }
};



module.exports.setUserToAdmin = async (reqBody, userData) => {
    if(userData.isAdmin){
        let updatedToAdmin = {
            userId: reqBody.id,
            isAdmin: reqBody.isAdmin
        }

        return await User.findByIdAndUpdate(reqBody.userId,
            updatedToAdmin).then((updatedToAdmin, error)=>{
            if(error){
                return false
            } else{
                return true
            }
        })

    } else {
       return false


    }
};

// 